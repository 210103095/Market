package com.example.test.dto;

import lombok.Data;

@Data
public class LoginDto {
    String username;
    String password;
}
